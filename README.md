# On-Road Vehicle PM2.5 correction factor
A correction factor to correct for the near-road PM2.5 emitted from on-road gasoline and diesel vehicles

Supporting Information for the dissertation Bridging the Gap Between Emission Simulators and Near-Road PM2.5 Measurements

There are two options for using the correction factor, either using a look-up table or through a Random Forest.

## Look-up Table
1. Download and install [MySQL](https://www.mysql.com/downloads/)
2. Create a MySQL database
3. Import [`CF_lookup_table.sql`](CF_lookup_table.sql) to database
4. Look up the correction factor based on variables that are known and similar to those used in the look-up table. In lieu of look-up tables or for values not listed within the lookup tables, the Random Forest can be used.

### Notes:
`Vehicle_Type`:
- `1` = Light duty vehicle
- `2` = Heavy duty vehicle

`Fuel_Type`:
- `1` = Gasoline
- `2` = Diesel

`Sampling_Type`:
- `1` = PEMS
- `2` = Dilution Tunnel

## Random Forest
### Setup
1. Download and install [Python version 3.9](https://www.python.org/downloads/)
2. In command window run `pip install pipenv`

### Run Random Forest Models
To run the regression tree to predict the correction factor needed please follow these steps:

1. Download and unzip <zip file here> into a new folder
2. Open the command window and change directory to the folder just created (`cd path-to-folder`)
3. Run `pipenv install`
4. Run `pipenv shell`
5. Run `CF_Prediction_RF.py`

#### Notes:
- All temperatures are in Kelvin
- All EC and OC are in ug/m3
- correction factor > 1 means traditional approach is under-predicting
- correction factor < 1 means traditional approach is over-predicting

#### User Inputs for `CF_Prediction_RF.py`

##### Scenarios: 
- `1` = LDGV PEMS -- light duty gasoline vehicle sampled using a PEMS
- `2` = LDGV Dilution Tunnel -- light duty gasoline vehicle sampled using a dilution tunnel (constant volume sampler + dyno)
- `3` = HDDV PEMS -- heavy duty diesel vehicle sampled using a PEMS
- `4` = HDDV Dilution Tunnel -- heavy duty diesel vehicle sampled using a dilution tunnel (constant volume sampler + dyno)

##### ROG Running Exhaust and CO2 Running Exhaust:
Using EMFAC get the running exhaust for the desired vehicle and model year. I.E., EMFAC HDDV model year 2000 (T7 tractor): ROG = `0.268800556` and CO2 = `1695.245793`

##### Sampling dilution ratio:
- PEMS: numeric value from `1` to `10` (i.e., 3)
- LDGV Dilution Tunnel: numeric value from `7` to `30` (i.e., 10)
- HDDV Dilution Tunnel: numeric value from `6` to `60` (i.e., 10)

##### Total dilution ratio:
Numeric value from `0` to `10500` (i.e., 1000)
  - Distance from vehicle. This varies depending on stability class and atmospheric conditions. For stability class C: DR 1000 ~ 8m and DR 10000 ~ 100m

##### Sampling temperature (Kelvin):
- PEMS: numeric value from `323.15` to `343.15` (i.e., 333.15)
- Dilution Tunnel: numeric value from `315.15` to `325.15` (i.e., 320.15)

##### Ambient temperature (Kelvin):
Numeric value from `258.15` to `313.15` (i.e., 293.15)

##### Vehicle EC:OC ratio:
Numeric value from `0` to `1` (i.e., 0.40)

##### Background ambient organic carbon concentration (ug/m3):
Numeric value from `0` to `10` (i.e., 4)