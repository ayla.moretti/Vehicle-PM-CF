# The following packages need to be installed in Python pipenv install -- see "readme.txt"
import joblib
from joblib import dump, load


def Predict_CF (a,x):
    x = [float(i) for i in x]
    try:
        if a == 1:
            clf = load('Random_Forest_model_LDGV_PEMS.joblib')
        elif a == 2:
            clf = load('Random_Forest_model_LDGV_CVS.joblib')
        elif a == 3:
            clf = load('Random_Forest_model_HDDV_PEMS.joblib')
        elif a == 4:
            clf = load('Random_Forest_model_HDDV_CVS.joblib')
    except:
        print('Double-check joblib file is in woking directory')
        quit()
    CF_pred = clf.predict([x])
    print('Correction Factor: ',CF_pred)
    return

def ROG_Function (fuel_type,ROG_run,CO2_run):
    try:
        if fuel_type == 'gasoline':
            Percent_CarbonDioxide = 1/(14.7+1) # Air to Fuel ratio
        elif fuel_type == 'diesel':
            Percent_CarbonDioxide = 1/(14.5+1) # Air to Fuel ratio
        CarbonDioxide_MW = 44.01 # g/mol
        T = 273.15 + 25 # temperature - K
        P = 101325 # Pressure - Pa
        R = 8.314 # gas constant - m3*Pa*mol^-1*K^-1
        Percent_CarbonDioxide_ug = Percent_CarbonDioxide*(((P*CarbonDioxide_MW)/(R*T))*10**6) # %CO2 to ug/m3 of CO2 of undiluted exaust
        HC_i = (ROG_run/CO2_run)*Percent_CarbonDioxide_ug
    except:
        print('Double-check input for desired scenario')
        quit()
    return (HC_i)

# 1 = LDGV PEMS
# 2 = LDGV Dilution Tunnel 
# 3 = HDDV PEMS
# 4 = HDDV Dilution Tunnel
# x = [ROG,DR,TDR,ST,AT,ECOC,BOASampling DR, Total DR,Sampling T,Ambient T,EC:OC,Background OC]

print('Please see readme.txt for more information on input variables')
a = input('Please enter desired scenario from 1 to 4: ')
ROG = input('Please enter desired ROG running exhaust: ')
CO2 = input('Please enter desired CO2 running exhaust: ')
DR = input('Please enter desired sampling dilution ratio: ')
TDR = input('Please enter desired total dilution ratio: ')
ST = input('Please enter desired sampling temperature (K): ')
AT = input('Please enter desired ambient temperature (K): ')
ECOC = input('Please enter desired vehicle EC:OC: ')
BOA = input('Please enter desired background ambient organic carbon concentration: ')

try:
    a = float(a)
    ROG = float(ROG)
    CO2 = float(CO2)
    DR = float(DR)
    TDR = float(TDR)
    ST = float(ST)
    AT = float(AT)
    ECOC = float(ECOC)
    BOA = float(BOA)
except:
    print('Please check input values and only input one numeric value per variable. See readme.txt for more information on input variables')
    quit()

if a == 1:
    fuel_type = 'gasoline'
elif a == 2:
    fuel_type = 'gasoline'
elif a == 3:
    fuel_type = 'diesel'
elif a == 4:
    fuel_type = 'diesel'

Predict_CF (a,[ROG_Function(fuel_type,ROG,CO2),DR,TDR,ST,AT,ECOC,BOA])
